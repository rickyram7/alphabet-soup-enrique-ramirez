/**
 *Author: Enrique E Ramirez
 * Date: 03/29/2023
 * Program name: Alphabet Soup
 */

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.io.*;

/**
 Steps:
 1. Receive an inputted text file containing the word search board and the words that need to be found ie:
 3x3
A B C
D E F
G H I
ABC
AEI
Assume that the file is inputted correctly
 REQUIREMENTS
 * Load a character grid with scrambled words embedded within it and a words list of the words to find.  The following conditions apply:
 *
 * Within the grid of characters, the words may appear vertical, horizontal or diagonal.
 * Within the grid of characters, the words may appear forwards or backwards.
 * Words that have spaces in them will not include spaces when hidden in the grid of characters.
 */
// Main class


public class Solver {

    public static void main(String[] args) {
        try {
            // Read the text file
            FileReader reader = new FileReader(args[0]);
            BufferedReader br = new BufferedReader(reader);
            // Read the first line of the text file, this line contains the number of rows and columns for the grid
            String line = br.readLine();
            // Parse the first line to get the number of rows and columns, the first character is the number of rows and the third character is the number of columns
            char rowsChar = line.charAt(0);
            int rows = Integer.parseInt(String.valueOf(rowsChar));
            char columnsChar = line.charAt(2);
            int columns = Integer.parseInt(String.valueOf(columnsChar));
            char[][] charGrid = new char[rows][columns];
            // Populate the character grid with the characters from the text file
            for (int i = 0; i < rows; i++) {
                line = br.readLine();
                line = line.replaceAll("\\s", "");
                for (int j = 0; j < columns; j++) {
                    charGrid[i][j] = line.charAt(j);
                }
            }
            // Read the rest of the text file and store the words in an array
            List<String> listOfStrings
                    = new ArrayList<String>();
            line = br.readLine();
            while (line != null) {
                listOfStrings.add(line);
                line = br.readLine();

            }
            reader.close();
            // Convert the list of strings to an array of strings
            String[] array
                    = listOfStrings.toArray(new String[0]);
            // Iterate through the array of words
            for (String word : array) {
                // Iterate through the character grid
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < columns; j++) {
                        // Check each position in the characterGrid for the first letter of the word
                        if (charGrid[i][j] == word.charAt(0)) {
                            // Check directions for the word
                            for (int rowIncrement = -1; rowIncrement <= 1; rowIncrement++) {
                                for (int columnIncrement = -1; columnIncrement <= 1; columnIncrement++) {
                                    // Only execute checkDirection method if there is a search in at least one direction
                                    if (!(rowIncrement ==0 && columnIncrement == 0)) {
                                        // Check if the word is in the direction specified by rowIncrement and columnIncrement
                                        if (checkDirection(charGrid, i, j, word, rowIncrement, columnIncrement)) {
                                            // Print the word and the starting and ending positions
                                            System.out.println(word + " " + i + ":" + j + " " + (i + rowIncrement * (word.length() - 1)) + ":" + (j + columnIncrement * (word.length() - 1)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // Method to check if the word is in the direction specified by rowIncrement and columnIncrement
    private static boolean checkDirection(char[][] grid, int i, int j, String word, int rowIncrement, int columnIncrement) {
        // Check if the word is in the direction specified by rowIncrement and columnIncrement
        for (int k = 1; k < word.length(); k++) {
            // Check if the next character in the word is out of bounds
            if (i + rowIncrement * k < 0 || i + rowIncrement * k >= grid.length || j + columnIncrement * k < 0 || j + columnIncrement * k >= grid[0].length) {
                return false;
            }
            // Check if the next character in the word does not match the character in the grid
            if (grid[i + rowIncrement * k][j + columnIncrement * k] != word.charAt(k)) {
                return false;
            }
        }
        return true;
    }
}